package model

type CommentLike struct {
	Id        string `json:"id"`
	CommentId string `json:"commentId"`
	Like      int    `json:"like"`
}
