package model

type Comment struct {
	Id           string        `json:"id"`
	MovieId      string        `json:"movieId"`
	Content      string        `json:"content"`
	CommentRates []CommentRate `json:"commentRates"`
	CommentLikes []CommentLike `json:"commentLikes"`
}
