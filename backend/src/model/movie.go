package model

import "time"

type Movie struct {
	Id               string    `json:"id"`
	Name             string    `json:"name"`
	Genre            string    `json:"genre"`
	Description      string    `json:"description"`
	Language         string    `json:"language"`
	Director         string    `json:"director"`
	NumOfAwards      int       `json:"numOfAwards"`
	MainActors       string    `json:"mainActors"`
	MovieLength      int       `json:"movieLength"`
	YearOfProduction time.Time `json:"yearOfProduction"`
	Budget           int       `json:"budget"`
	Rates            []Rate    `json:"rates"`
	Comments         []Comment `json:"comments"`
}
