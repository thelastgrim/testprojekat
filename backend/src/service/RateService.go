package service

import (
	"errors"
	"model"
	"repository"

	"github.com/google/uuid"
)

type RateService interface {
	Validate(rate *model.Rate) error
	Save(rate *model.Rate) (*model.Rate, error)
}

type rateService struct{}

var (
	rateRepo repository.RateRepository = repository.NewRateRepository()
)

func NewRateService() RateService {
	return &rateService{}
}

func (*rateService) Save(rate *model.Rate) (*model.Rate, error) {

	rate.Id = uuid.New().String()

	return rateRepo.Save(rate)
}

func (*rateService) Validate(rate *model.Rate) error {

	if rate.MovieId == "" {
		error := errors.New("Rate MovieId cannot be empty.")
		return error
	}

	if rate.Mark < 1 {
		error := errors.New("Rate Mark cannot be less than 1.")
		return error
	}

	if rate.Mark > 5 {
		error := errors.New("Rate Mark cannot be greater than 5.")
		return error
	}

	return nil
}
