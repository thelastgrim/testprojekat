package controller

import (
	"encoding/json"
	"fmt"
	"model"
	"net/http"
	"service"
)

type MovieController interface {
	GetAll(res http.ResponseWriter, req *http.Request)
	GetAllFilter(res http.ResponseWriter, req *http.Request)
}

type movieController struct{}

var (
	movieService service.MovieService = service.NewMovieService()
)

func NewMovieController() MovieController {
	return &movieController{}
}

func (*movieController) GetAllFilter(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Content-Type", "application/json")

	var filter model.Filter

	error_decode := json.NewDecoder(req.Body).Decode(&filter)

	if error_decode != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "Error request body decoding"})

		fmt.Println("*** Error request body decoding ***")

		return
	}

	movies, err := movieService.FindAllFilter(&filter)

	if err != nil {
		res.WriteHeader(500)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "Error getting the movies"})
	}

	res.WriteHeader(200)
	json.NewEncoder(res).Encode(movies)
}

func (*movieController) GetAll(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Content-Type", "application/json")

	movies, err := movieService.FindAll()

	if err != nil {
		res.WriteHeader(500)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "Error getting the movies"})
	}

	res.WriteHeader(200)
	json.NewEncoder(res).Encode(movies)
}
