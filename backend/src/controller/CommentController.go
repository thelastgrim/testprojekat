package controller

import (
	"encoding/json"
	"fmt"
	"model"
	"net/http"
	"service"
)

type CommentController interface {
	Save(response http.ResponseWriter, request *http.Request)
	SaveLike(response http.ResponseWriter, request *http.Request)
	SaveRate(response http.ResponseWriter, request *http.Request)
}

type commentController struct{}

var (
	commentService service.CommentService = service.NewCommentService()
)

func NewCommentController() CommentController {
	return &commentController{}
}

func (*commentController) Save(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Content-Type", "application/json")

	var movieComm model.Comment

	error_decode := json.NewDecoder(req.Body).Decode(&movieComm)

	if error_decode != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "Error request body decoding"})

		fmt.Println("*** Error request body decoding ***")

		return
	}

	error_validate := commentService.Validate(&movieComm)

	if error_validate != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: error_validate.Error()})

		fmt.Println(error_validate.Error())

		return
	}

	result, error_service_create := commentService.Save(&movieComm)

	if error_service_create != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "*** Error saving the Movie Comment ***"})

		return
	}

	res.WriteHeader(201)
	json.NewEncoder(res).Encode(result)
}

func (*commentController) SaveLike(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Content-Type", "application/json")

	var commLike model.CommentLike

	error_decode := json.NewDecoder(req.Body).Decode(&commLike)

	if error_decode != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "Error request body decoding"})

		fmt.Println("*** Error request body decoding ***")

		return
	}

	error_validate := commentService.ValidateLike(&commLike)

	if error_validate != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: error_validate.Error()})

		fmt.Println(error_validate.Error())

		return
	}

	result, error_service_create := commentService.SaveLike(&commLike)

	if error_service_create != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "*** Error saving the Movie Comment Like ***"})

		return
	}

	res.WriteHeader(201)
	json.NewEncoder(res).Encode(result)
}

func (*commentController) SaveRate(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Content-Type", "application/json")

	var commRate model.CommentRate

	error_decode := json.NewDecoder(req.Body).Decode(&commRate)

	if error_decode != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "Error request body decoding"})

		fmt.Println("*** Error request body decoding ***")

		return
	}

	error_validate := commentService.ValidateRate(&commRate)

	if error_validate != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: error_validate.Error()})

		fmt.Println(error_validate.Error())

		return
	}

	result, error_service_create := commentService.SaveRate(&commRate)

	if error_service_create != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "*** Error saving the Movie Comment Like ***"})

		return
	}

	res.WriteHeader(201)
	json.NewEncoder(res).Encode(result)
}
