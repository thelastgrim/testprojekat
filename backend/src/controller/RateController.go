package controller

import (
	"encoding/json"
	"fmt"
	"model"
	"net/http"
	"service"
)

type RateController interface {
	Save(response http.ResponseWriter, request *http.Request)
}

type rateController struct{}

var (
	rateService service.RateService = service.NewRateService()
)

func NewRateController() RateController {
	return &rateController{}
}

func (*rateController) Save(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Content-Type", "application/json")

	var movieRate model.Rate

	error_decode := json.NewDecoder(req.Body).Decode(&movieRate)

	if error_decode != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "Error request body decoding"})

		fmt.Println("*** Error request body decoding ***")

		return
	}

	error_validate := rateService.Validate(&movieRate)

	if error_validate != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: error_validate.Error()})

		fmt.Println(error_validate.Error())

		return
	}

	result, error_service_create := rateService.Save(&movieRate)

	if error_service_create != nil {
		res.WriteHeader(400)
		json.NewEncoder(res).Encode(model.ServiceError{Message: "*** Error saving the Movie Rate ***"})

		return
	}

	res.WriteHeader(201)
	json.NewEncoder(res).Encode(result)
}
