package repository

import (
	"database/sql"
	"fmt"
	"model"

	_ "github.com/lib/pq"
)

type RateRepository interface {
	FindAllByMovieId(movieId string) ([]model.Rate, error)
	Save(comment *model.Rate) (*model.Rate, error)
}

type rateRepo struct{}

func NewRateRepository() RateRepository {
	return &rateRepo{}
}

func (*rateRepo) FindAllByMovieId(movieId string) ([]model.Rate, error) {
	dbConnetion := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", dbConnetion)
	CheckError(err)

	rows, err := db.Query(`SELECT "id", "movieId", "mark" FROM "Rate" WHERE "movieId" = $1`, movieId)
	CheckError(err)

	// var movies []model.Movie
	rates := []model.Rate{}

	for rows.Next() {
		var id string
		var movieId string
		var mark int

		err = rows.Scan(&id, &movieId, &mark)
		CheckError(err)

		rates = append(rates, model.Rate{Id: id, MovieId: movieId, Mark: mark})
	}

	defer db.Close()

	defer rows.Close()

	return rates, nil
}

func (*rateRepo) Save(rate *model.Rate) (*model.Rate, error) {
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlconn)
	CheckError(err)

	insertStmt := `insert into "Rate"("id", "movieId", "mark") values($1, $2, $3)`
	_, e := db.Exec(insertStmt, rate.Id, rate.MovieId, rate.Mark)
	CheckError(e)

	defer db.Close()

	return rate, nil
}
