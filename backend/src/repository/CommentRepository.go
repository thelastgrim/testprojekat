package repository

import (
	"database/sql"
	"fmt"
	"model"

	_ "github.com/lib/pq"
)

type CommentRepository interface {
	FindAllByMovieId(movieId string) ([]model.Comment, error)
	Save(comment *model.Comment) (*model.Comment, error)
}

type commentRepo struct{}

func NewCommentRepository() CommentRepository {
	return &commentRepo{}
}

var (
	commRateRepo CommentRateRepository = NewCommentRateRepository()
	commLikeRepo CommentLikeRepository = NewCommentLikeRepository()
)

func (*commentRepo) FindAllByMovieId(movieId string) ([]model.Comment, error) {
	dbConnetion := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", dbConnetion)
	CheckError(err)

	rows, err := db.Query(`SELECT "id", "movieId", "content" FROM "Comment" WHERE "movieId" = $1`, movieId)
	CheckError(err)

	// var movies []model.Movie
	comments := []model.Comment{}

	for rows.Next() {
		var id string
		var movieId string
		var content string

		err = rows.Scan(&id, &movieId, &content)
		CheckError(err)

		commentRates, _ := commRateRepo.FindAllByCommentId(id)
		commentLikes, _ := commLikeRepo.FindAllByCommentId(id)

		comments = append(comments, model.Comment{Id: id, MovieId: movieId, Content: content, CommentRates: commentRates, CommentLikes: commentLikes})
	}

	defer db.Close()

	defer rows.Close()

	return comments, nil
}

func (*commentRepo) Save(comment *model.Comment) (*model.Comment, error) {
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlconn)
	CheckError(err)

	insertStmt := `insert into "Comment"("id", "movieId", "content") values($1, $2, $3)`
	_, e := db.Exec(insertStmt, comment.Id, comment.MovieId, comment.Content)
	CheckError(e)

	defer db.Close()

	return comment, nil
}
