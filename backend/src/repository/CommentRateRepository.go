package repository

import (
	"database/sql"
	"fmt"
	"model"

	_ "github.com/lib/pq"
)

type CommentRateRepository interface {
	FindAllByCommentId(movieId string) ([]model.CommentRate, error)
	Save(rate *model.CommentRate) (*model.CommentRate, error)
}

type commentRateRepo struct{}

func NewCommentRateRepository() CommentRateRepository {
	return &commentRateRepo{}
}

func (*commentRateRepo) FindAllByCommentId(commentId string) ([]model.CommentRate, error) {
	dbConnetion := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", dbConnetion)
	CheckError(err)

	rows, err := db.Query(`SELECT "id", "commentId", "mark" FROM "CommentRate" WHERE "commentId" = $1`, commentId)
	CheckError(err)

	// var movies []model.Movie
	rates := []model.CommentRate{}

	for rows.Next() {
		var id string
		var commentId string
		var mark int

		err = rows.Scan(&id, &commentId, &mark)
		CheckError(err)

		rates = append(rates, model.CommentRate{Id: id, CommentId: commentId, Mark: mark})
	}

	defer db.Close()

	defer rows.Close()

	return rates, nil
}

func (*commentRateRepo) Save(commentRate *model.CommentRate) (*model.CommentRate, error) {
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlconn)
	CheckError(err)

	insertStmt := `insert into "CommentRate"("id", "commentId", "mark") values($1, $2, $3)`
	_, e := db.Exec(insertStmt, commentRate.Id, commentRate.CommentId, commentRate.Mark)
	CheckError(e)

	defer db.Close()

	return commentRate, nil
}
