package repository

import (
	"database/sql"
	"fmt"
	"model"
	"strings"
	"time"

	_ "github.com/lib/pq"
)

type MovieRepository interface {
	FindAll() ([]model.Movie, error)
	FindAllFilter(filter *model.Filter) ([]model.Movie, error)
}

type movieRepo struct{}

func NewMovieRepository() MovieRepository {
	return &movieRepo{}
}

var (
	commRepo  CommentRepository = NewCommentRepository()
	rateRepos RateRepository    = NewRateRepository()
)

func (*movieRepo) FindAllFilter(filter *model.Filter) ([]model.Movie, error) {

	dbConnetion := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", dbConnetion)
	CheckError(err)

	rows, err := db.Query(`SELECT "id", "name", "genre", "description", "language", "director", "numOfAwards", 
	"mainActors", "movieLength", "yearOfProduction", "budget" FROM "Movie" ` + querySort(filter))
	CheckError(err)

	movies := []model.Movie{}

	for rows.Next() {
		var id string
		var name string
		var genre string
		var description string
		var language string
		var director string
		var numOfAwards int
		var mainActors string
		var movieLength int
		var yearOfProduction string
		var budget int

		err = rows.Scan(&id, &name, &genre, &description, &language, &director, &numOfAwards, &mainActors, &movieLength, &yearOfProduction, &budget)
		CheckError(err)

		comments, _ := commRepo.FindAllByMovieId(id)
		rates, _ := rateRepos.FindAllByMovieId(id)

		const layout = "2006-01-02"
		yopDate, _ := time.Parse(layout, yearOfProduction[0:10])

		movies = append(movies, model.Movie{Id: id, Name: name, Genre: genre, Description: description, Language: language,
			Director: director, NumOfAwards: numOfAwards, MainActors: mainActors, MovieLength: movieLength,
			YearOfProduction: yopDate, Budget: budget, Rates: rates, Comments: comments})
	}

	defer db.Close()

	defer rows.Close()

	movies = getMoviesByName(filter, movies)
	movies = getMoviesByGenre(filter, movies)
	movies = getMoviesByDescription(filter, movies)
	movies = getMoviesByLanguage(filter, movies)
	movies = getMoviesByDirector(filter, movies)
	movies = getMoviesByMainActors(filter, movies)
	movies = getMoviesByMovieLength(filter, movies)
	movies = getMoviesByBudget(filter, movies)
	movies = getMoviesByNumOfAwards(filter, movies)
	movies = getMoviesByDate(filter, movies)

	return movies, nil
}

func querySort(filter *model.Filter) string {
	var query = ""

	if filter.SortBy == "nameASC" {
		query = `ORDER BY "name" ASC`
	} else if filter.SortBy == "nameDESC" {
		query = `ORDER BY "name" DESC`
	} else if filter.SortBy == "budgetASC" {
		query = `ORDER BY "budget" ASC`
	} else if filter.SortBy == "budgetDESC" {
		query = `ORDER BY "budget" DESC`
	} else if filter.SortBy == "yearOfProductionASC" {
		query = `ORDER BY "yearOfProduction" ASC`
	} else if filter.SortBy == "yearOfProductionDESC" {
		query = `ORDER BY "yearOfProduction" DESC`
	}

	return query
}

func getMoviesByDate(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	const layout = "2006-01-02"
	dateFrom := filter.YearOfProductionFrom
	dateTo := filter.YearOfProductionTo

	for i := 0; i < len(movies)+delNumber; i++ {
		moviDate := movies[i-delNumber].YearOfProduction
		if !(moviDate.After(dateFrom) && moviDate.Before(dateTo)) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByBudget(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !(movies[i-delNumber].Budget >= filter.BudgetFrom && movies[i-delNumber].Budget <= filter.BudgetTo) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByNumOfAwards(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !(movies[i-delNumber].NumOfAwards <= filter.NumOfAwards) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByMovieLength(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !(movies[i-delNumber].MovieLength >= filter.MovieLengthFrom && movies[i-delNumber].MovieLength <= filter.MovieLengthTo) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByMainActors(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !strings.Contains(strings.ToLower(movies[i-delNumber].MainActors), strings.ToLower(filter.MainActors)) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByDirector(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !strings.Contains(strings.ToLower(movies[i-delNumber].Director), strings.ToLower(filter.Director)) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByLanguage(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !strings.Contains(strings.ToLower(movies[i-delNumber].Language), strings.ToLower(filter.Language)) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByDescription(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0 // number of removing elem from list

	for i := 0; i < len(movies)+delNumber; i++ {
		if !strings.Contains(strings.ToLower(movies[i-delNumber].Description), strings.ToLower(filter.Description)) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByGenre(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !strings.Contains(strings.ToLower(movies[i-delNumber].Genre), strings.ToLower(filter.Genre)) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func getMoviesByName(filter *model.Filter, movies []model.Movie) []model.Movie {
	var delNumber = 0

	for i := 0; i < len(movies)+delNumber; i++ {
		if !strings.Contains(strings.ToLower(movies[i-delNumber].Name), strings.ToLower(filter.Name)) {
			movies = removeElemFromList(movies, i-delNumber)
			delNumber = delNumber + 1
		}
	}

	return movies
}

func removeElemFromList(movies []model.Movie, i int) []model.Movie {
	movies[i] = movies[len(movies)-1]
	return movies[:len(movies)-1]
}

/*
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
---------------------------------------
*/
func (*movieRepo) FindAll() ([]model.Movie, error) {

	dbConnetion := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", dbConnetion)
	CheckError(err)

	rows, err := db.Query(`SELECT "id", "name", "genre", "description", "language", "director", "numOfAwards", 
	"mainActors", "movieLength", "yearOfProduction", "budget" FROM "Movie"`)
	CheckError(err)

	// var movies []model.Movie
	movies := []model.Movie{}

	// rates := []model.Rate{}
	// comments := []model.Comment{}

	for rows.Next() {
		var id string
		var name string
		var genre string
		var description string
		var language string
		var director string
		var numOfAwards int
		var mainActors string
		var movieLength int
		var yearOfProduction string
		var budget int

		err = rows.Scan(&id, &name, &genre, &description, &language, &director, &numOfAwards, &mainActors, &movieLength, &yearOfProduction, &budget)
		CheckError(err)

		comments, _ := commRepo.FindAllByMovieId(id)
		rates, _ := rateRepos.FindAllByMovieId(id)

		const layout = "2006-01-02"
		yopDate, _ := time.Parse(layout, yearOfProduction[0:10])

		movies = append(movies, model.Movie{Id: id, Name: name, Genre: genre, Description: description, Language: language,
			Director: director, NumOfAwards: numOfAwards, MainActors: mainActors, MovieLength: movieLength,
			YearOfProduction: yopDate, Budget: budget, Rates: rates, Comments: comments})
	}

	defer db.Close()

	defer rows.Close()

	return movies, nil
}

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
