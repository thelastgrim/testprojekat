package main

import (
	"controller"
	"fmt"
	"net/http"

	"router"
)

var (
	movieController   controller.MovieController   = controller.NewMovieController()
	commentController controller.CommentController = controller.NewCommentController()
	rateController    controller.RateController    = controller.NewRateController()

	httpRouter router.Router = router.NewMuxRouter()
)

func runServer() {
	const port string = ":8000"

	httpRouter.GET("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(w, "Running!")
	})

	httpRouter.GET("/movies", movieController.GetAll)
	httpRouter.POST("/movies_filter", movieController.GetAllFilter)

	httpRouter.POST("/comments", commentController.Save)
	httpRouter.POST("/comments_like", commentController.SaveLike)
	httpRouter.POST("/comments_rate", commentController.SaveRate)
	httpRouter.POST("/rates", rateController.Save)

	httpRouter.SERVE(port)
}
